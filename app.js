const express = require("express");

const app = express();
const port = process.env.PORT || 3001;

const testResult = JSON.stringify({ "name": "test app", "message": "Hello World" })

app.get("/", (req, res) => res.type('json').send(testResult))

app.listen(port, () => console.log(`App listening on port ${port}!`));